﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tételek_LINQ
{
    class Program
    {
        static public List<int> adatok = new List<int>();
        static public void generál(int méret)
        {   //adatbeolvasás helyett egy véletlenszámokat generálunk
            Random r = new Random();
            for (int i = 0; i < méret; i++)
            {
                adatok.Add(r.Next(100));
            }
        }
        static public int összegzés_hagyományosan()
        {
            int s = 0;
            for (int i = 0; i < adatok.Count; i++)
                s += adatok[i];
            return s;
        }
        static public int összegzés_LINQ()
        {
            return adatok.Sum();
        }
        static public bool eldöntés_hagyományosan(Func<int,bool> T) //delegált használat
        {
            int i= 0;
            while (i < adatok.Count && !T(adatok[i]))
                i++;
            return i<adatok.Count();
        }
        static public bool eldöntés_LINQ(Func<int, bool> T)
        {
            return adatok.FindIndex(x=>T(x)==true)>=0; //-1, ha nincs
        }
        static int megszámolás_hagyományosan(Func<int, bool> T)
        {
            int db = 0;
            for(int i=0;i<adatok.Count;i++)
            {
                if (T(adatok[i])) db++;
            }
            return db;
        }
        static int megszámolás_LINQ(Func<int, bool> T)
        {
            return adatok.Where(x=>T(x)==true).Count();
        }
        static int kiválasztás_hagyományosan(Func<int, bool> T)
        {
            int i = 0;
            while (!T(adatok[i]))
                i++;
            return adatok[i];
        }
        static int kiválasztás_LINQ(Func<int, bool> T) //értéket adunk vissza
        {
            return adatok.Where(x => T(x) == true).First();
        }
        static int maximumkiválasztás_hagyományosan(Func<int, bool> T)
        {
            int maxérték = adatok[0];
            for (int i=1;i<adatok.Count();i++)
            {
                if (maxérték < adatok[i]) maxérték = adatok[i];
            }
            return maxérték;
        }
        static int maximumkiválasztás_LINQ(Func<int, bool> T)
        {
            return adatok.Max();
        }
        static (bool,int) keresés_hagyományosan(Func<int, bool> T)
        {
            int i = 0;
            while (i < adatok.Count && !T(adatok[i]))
                i++;
            return (i < adatok.Count, adatok[i]);
        }
        static (bool, int) keresés_LINQ(Func<int, bool> T) //tuple
        {
            return adatok.Where(x => T(x) == true).Select(x => (adatok.IndexOf(x) > -1,  x )).First();
        }
        static List<int> kiválogatás_hagyományosan(Func<int, bool> T)
        {
            List<int> eredmény = new List<int>();
            for (int i=0;i<adatok.Count();i++)
            {
                if (T(adatok[i]))
                    eredmény.Add(adatok[i]);
            }
            return eredmény;
        }
        static void kiírás(List<int> lista)
        {
            foreach (int elem in adatok)
            {
                Console.Write(elem + ",");
            }
            Console.WriteLine();
        }
        static List<int> kiválogatás_LINQ(Func<int, bool> T)
        {
            return adatok.Where(x => T(x) == true).ToList();
        }
        static void Main(string[] args)
        {
            generál(10);
            kiírás(adatok);
            Console.WriteLine("Hagyományos, Az elemek összege: {0}",összegzés_hagyományosan());
            Console.WriteLine("LINQ-s, Az elemek összege: {0}", összegzés_LINQ());
            if (eldöntés_hagyományosan(x => x > 5)) { Console.WriteLine("Hagyományosan, Eldöntés:  Van"); }
            else { Console.WriteLine("Hagyományosan, Eldöntés: Nincs"); }
            if (eldöntés_LINQ(x => x > 5)) { Console.WriteLine("LINQ-val, Eldöntés:  Van"); }
            else { Console.WriteLine("LINQ-val, Eldöntés: Nincs"); }
            Console.WriteLine("Hagyományos, megszámolás 5-nél nagyobb: {0}", megszámolás_hagyományosan((x => x > 5)));
            Console.WriteLine("LINQ-s, megszámolás 5-nél nagyobb: {0}", megszámolás_LINQ((x => x > 5)));
            Console.WriteLine("Hagyományos, kiválasztás első 5-nél nagyobb: {0}", kiválasztás_hagyományosan((x => x > 5)));
            Console.WriteLine("LINQ-s, kiválasztás első 5-nél nagyobb: {0}", kiválasztás_LINQ((x => x > 5)));
            Console.WriteLine("Hagyományos, maximumkiválasztás: {0}", maximumkiválasztás_hagyományosan((x => x > 5)));
            Console.WriteLine("LINQ-s, maximumkiválasztás: {0}", maximumkiválasztás_LINQ((x => x > 5)));
            (bool van, int elem) = keresés_hagyományosan((x => x > 5));
            Console.Write("Hagyományosan, keresés van-e 5-nél nagyobb és melyik: ");
            if (van) { Console.WriteLine(" Van és {0}", elem); }
            else { Console.WriteLine("Nincs"); }
            (van, elem) = keresés_LINQ((x => x > 5));
            Console.Write("LINQ-val, keresés van-e 5-nél nagyobb és melyik: ");
            if (van) { Console.WriteLine(" Van és {0}", elem); }
            else { Console.WriteLine("Nincs"); }
            List<int> eredmény = kiválogatás_hagyományosan(x => x > 5);
            Console.WriteLine("Hagyományos, kiválogatás 5-nél nagyobbak: ");
            kiírás(eredmény);
            eredmény = kiválogatás_LINQ(x => x > 5);
            Console.WriteLine("LINQ-val, kiválogatás 5-nél nagyobbak: ");
            kiírás(eredmény);
           
        }
    }
}
